# OCR

## Описание

## Демо

- Установить окружение:
``` shell
> export DOCKER_UID=1000
> export DOCKER_GID=1000
> docker-compose build
```

Запуск демо:
``` shell
> docker-compose run workspace
> python3 ./scripts/demo.py
```

- Поместить .ckpt файл модели по пути `data/model.ckpt`.

- Запустить демо. Результирующее изображение будет сохранено по пути `demo/result.png`
``` shell
> python3 -m scripts.demo
```

![Demo](doc/demo.png)

## Обучение 

- Скачать датасет IIIT5k: https://cvit.iiit.ac.in/research/projects/cvit-projects/the-iiit-5k-word-dataset

- Запустить скрипт:
``` shell
> python3 -m scripts.train
```


## Результаты

Был проведён эксперимент на смешанном датасете IIIT5k + Capcha. Capcha датасет был сгенерирован с
фиксированным сидом, что позволяет воспроизводить обучение. Сохранялась лучшая модель (49 эпоха).

Train loss:

![Train loss](doc/train_loss.png "train_loss")


Validation loss:

![Val loss](doc/val_loss.png "val_loss")


Validation accuracy:

![Accuracy](doc/acc.png "accuracy")


Validation Char Error Rate:

![CER](doc/cer.png "cer")



Значения метрик для лучшей модели на датасете Capcha:

![](doc/metrics.png)
