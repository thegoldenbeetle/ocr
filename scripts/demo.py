from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from src.dataset import IIIT5kDataModule
from src.model import CRNN
from src.utils import decode_raw_text

matplotlib.use("WebAgg")


if __name__ == "__main__":
    model = CRNN.load_from_checkpoint("data/model.ckpt")
    model.eval()

    datamodule = IIIT5kDataModule(
        "./data/IIIT5K",
        batch_size=1,
        capcha_train_samples=1,
        capcha_train_seed=None,
        capcha_test_samples=2,
        capcha_test_seed=None,
        only_capcha=True,
    )
    datamodule.setup()
    dataloader = datamodule.val_dataloader()

    item = next(iter(dataloader))
    img = item.orig_img[0]
    plt.imshow((np.array(img) * 255).astype(int))
    x = item.img
    pred = model(x)[0]
    raw_word = "".join(model.charencoder.inverse_transform([x.argmax() for x in pred]))
    pred_word = decode_raw_text(raw_word)
    print("Predict:", pred_word)
    print("Target:", item.text[0])
    plt.title(
        f"""
    Target: {"-".join(item.text[0])}
    Predict: {"-".join(pred_word)}
    """
    )

    Path("./demo/").mkdir(parents=True, exist_ok=True)
    plt.savefig("./demo/result.png", dpi=300)
    print("Save image to './demo/result.png'")
