from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from src.dataset import IIIT5kDataModule
from src.model import CRNN
from src.utils import decode_raw_text
from lightning.pytorch import Trainer, seed_everything
from lightning.pytorch.callbacks import ModelCheckpoint


matplotlib.use("WebAgg")


if __name__ == "__main__":
    model = CRNN.load_from_checkpoint("data/model.ckpt")
    model.eval()

    datamodule = IIIT5kDataModule(
        "./data/IIIT5K",
        batch_size=1,
        capcha_train_samples=1,
        capcha_train_seed=None,
        capcha_test_samples=1000,
        capcha_test_seed=3241,
        only_capcha=True,
    )
    checkpoint_callback = ModelCheckpoint(monitor="val/loss")
    trainer = Trainer(
        default_root_dir="logs",
        max_epochs=1,
        log_every_n_steps=10,
        callbacks=[checkpoint_callback],
        accelerator="cpu",
    )
    trainer.validate(model, datamodule)
