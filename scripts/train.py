import torch
from lightning.pytorch import Trainer, seed_everything
from lightning.pytorch.callbacks import ModelCheckpoint

from src.dataset import IIIT5kDataModule
from src.model import CRNN

if __name__ == "__main__":
    seed_everything(8324, workers=True)
    torch.set_float32_matmul_precision("high")
    model = CRNN()
    datamodule = IIIT5kDataModule(
        "./data/IIIT5K",
        batch_size=64,
        capcha_train_samples=2000,
        capcha_train_seed=54123,
        capcha_test_samples=1000,
        capcha_test_seed=3241,
        only_capcha=False,
    )
    checkpoint_callback = ModelCheckpoint(monitor="val/loss")
    trainer = Trainer(
        default_root_dir="logs",
        max_epochs=150,
        log_every_n_steps=10,
        callbacks=[checkpoint_callback],
    )
    trainer.fit(model, datamodule)
