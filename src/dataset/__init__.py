from .capcha import CapchaDataset
from .datamodule import IIIT5kDataModule
from .iiit5k import IIIT5k

__all__ = [
    IIIT5k,
    IIIT5kDataModule,
    CapchaDataset,
]
