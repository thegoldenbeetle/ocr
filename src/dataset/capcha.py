from typing import Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision.transforms.functional as TF
from PIL import Image
from torch.utils.data import Dataset
from torchvision import datasets, transforms

from .common import OCRDataBatch


class CapchaDataset(Dataset):
    """
    Датасет генерирует капчу длины seq_len из набора данных EMNIST
    """

    def __init__(
        self,
        seq_len: Union[int, Tuple[int, int]],
        img_h: int = 32,
        img_w: int = 28,
        split: str = "digits",
        samples: int = None,
        transform: Optional = None,
        pregenerate: bool = False,
        seed: Optional[bool] = None,
    ):
        self.generator = np.random.default_rng(seed)
        self.transform = transform
        self.pregenerate = pregenerate
        self.emnist_dataset = datasets.EMNIST(
            "./EMNIST", split=split, train=True, download=True
        )
        self.seq_len = seq_len
        self.blank_label = len(self.emnist_dataset.classes)
        self.img_h = img_h
        self.img_w = img_w
        self.samples = samples
        self.num_classes = len(self.emnist_dataset.classes) + 1
        if isinstance(seq_len, int):
            self._min_seq_len = seq_len
            self._max_seq_len = seq_len
        elif (
            isinstance(seq_len, Tuple)
            and len(seq_len) == 2
            and isinstance(seq_len[0], int)
        ):
            self._min_seq_len = seq_len[0]
            self._max_seq_len = seq_len[1]

        if self.pregenerate:
            self.pregenerate_data = [
                (
                    (
                        random_seq_len := self.generator.integers(
                            self._min_seq_len, self._max_seq_len + 1
                        )
                    ),
                    self.generator.integers(
                        len(self.emnist_dataset.data), size=(random_seq_len,)
                    ),
                )
                for _ in range(len(self))
            ]

    def __len__(self):
        """
        Можно нагенерировать N различных капчей, где N - число сочетаний с повторениями.
        Если задано samples - вернуть его
        """
        if self.samples is not None:
            return self.samples
        return len(self.emnist_dataset.classes) ** self._max_seq_len

    def __preprocess(self, random_images: torch.Tensor) -> np.ndarray:
        transformed_images = []
        for img in random_images:
            img = transforms.ToPILImage()(img)
            img = TF.rotate(img, -90, fill=[0.0])
            img = TF.hflip(img)
            img = TF.resize(img, size=(self.img_h, self.img_w))
            img = transforms.ToTensor()(img).numpy()
            transformed_images.append(img)
        images = np.array(transformed_images)
        images = np.hstack(
            images.reshape((len(transformed_images), self.img_h, self.img_w))
        )
        full_img = np.zeros(shape=(self.img_h, self._max_seq_len * self.img_w)).astype(
            np.float32
        )
        full_img[:, 0 : images.shape[1]] = images
        full_img = Image.fromarray(full_img, mode="F")
        return full_img

    def __getitem__(self, idx):
        if self.pregenerate:
            random_seq_len, random_indices = self.pregenerate_data[idx]
        else:
            # Get random seq_len
            random_seq_len = self.generator.integers(
                self._min_seq_len, self._max_seq_len + 1
            )
            # Get random ind
            random_indices = self.generator.integers(
                len(self.emnist_dataset.data), size=(random_seq_len,)
            )
        random_images = self.emnist_dataset.data[random_indices]
        random_digits_labels = self.emnist_dataset.targets[random_indices]
        labels = torch.zeros((1, self._max_seq_len))
        labels = torch.fill(labels, self.blank_label)
        labels[0, 0 : len(random_digits_labels)] = random_digits_labels

        orig_img = self.__preprocess(random_images)
        if self.transform:
            img = self.transform(orig_img)
        y = labels.numpy().reshape(self._max_seq_len)
        text = "".join([str(int(c)) for c in y if c < 10])
        return OCRDataBatch(
            img=img,
            orig_img=[orig_img],
            text=[text],
            lexicon=[None],
        )
