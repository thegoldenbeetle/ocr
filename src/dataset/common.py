import itertools
from dataclasses import dataclass
from typing import List, Set

import torch
from PIL import Image


@dataclass
class OCRDataBatch:
    img: torch.Tensor
    orig_img: List[Image.Image]
    text: List[str]
    lexicon: List[Set[str]]


def ocr_collate_fun(data):
    max([x.img.shape[2] for x in data])
    new_imgs = [x.img for x in data]
    return OCRDataBatch(
        img=torch.stack(new_imgs),
        orig_img=list(itertools.chain.from_iterable(x.orig_img for x in data)),
        text=list(itertools.chain.from_iterable(x.text for x in data)),
        lexicon=list(itertools.chain.from_iterable(x.lexicon for x in data)),
    )
