import os
from pathlib import Path
from typing import Optional, Tuple, Union

import torch
from lightning import LightningDataModule
from torch.utils.data import ConcatDataset, DataLoader
from torchvision import transforms

from .capcha import CapchaDataset
from .common import ocr_collate_fun
from .iiit5k import IIIT5k


class IIIT5kDataModule(LightningDataModule):
    def __init__(
        self,
        data_dir: Union[str, Path],
        train_transform=None,
        test_transform=None,
        batch_size: int = 64,
        image_size: Tuple[int, int] = (32, 400),
        lexicon: Optional[str] = "smallLexi",
        num_workers: Optional[int] = None,
        capcha_train_samples: Optional[int] = 1000,
        capcha_train_seed: Optional[int] = None,
        capcha_test_samples: Optional[int] = 1000,
        capcha_test_seed: Optional[int] = None,
        only_capcha: bool = False,
    ):
        super().__init__()

        if not (not only_capcha or (capcha_train_samples and capcha_test_samples)):
            raise ValueError(
                "If only_capcha set neet to set capcha_train_data and capcha_test_data"
            )

        self.data_dir = Path(data_dir)
        self.lexicon = lexicon
        self.batch_size = batch_size
        self.image_size = image_size
        self.num_workers = num_workers
        if num_workers is None:
            self.num_workers = os.cpu_count()

        self.only_capcha = only_capcha
        self.capcha_train_samples = capcha_train_samples
        self.capcha_train_seed = capcha_train_seed
        self.capcha_test_samples = capcha_test_samples
        self.capcha_test_seed = capcha_test_seed

        self.train_transform = train_transform
        if train_transform is None:
            self.train_transform = self.default_train_transform()

        self.test_transform = test_transform
        if test_transform is None:
            self.test_transform = self.default_test_transform()

    def default_train_transform(self):
        return transforms.Compose(
            [
                transforms.RandomApply([transforms.GaussianBlur(3)], p=0.3),
                transforms.RandomApply([transforms.RandomRotation(degrees=10)], p=0.3),
                transforms.ToTensor(),
                transforms.Grayscale(num_output_channels=1),
                transforms.Resize(self.image_size, antialias=True),
            ]
        )

    def default_test_transform(self):
        return transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Grayscale(num_output_channels=1),
                transforms.Resize(self.image_size, antialias=True),
            ]
        )

    def setup(self, stage=None):
        del stage
        if self.only_capcha:
            self.train_data = CapchaDataset(
                (3, 15),
                samples=self.capcha_train_samples,
                transform=self.train_transform,
                seed=self.capcha_train_seed,
                pregenerate=True,
            )
        else:
            self.train_data = IIIT5k(
                self.data_dir,
                train=True,
                lexicon=self.lexicon,
                transform=self.train_transform,
            )
            if self.capcha_train_samples:
                self.train_data = ConcatDataset(
                    (
                        self.train_data,
                        CapchaDataset(
                            (3, 15),
                            samples=self.capcha_train_samples,
                            transform=self.train_transform,
                            seed=self.capcha_train_seed,
                            pregenerate=True,
                        ),
                    )
                )
        if self.only_capcha:
            self.val_data = CapchaDataset(
                (3, 15),
                samples=self.capcha_test_samples,
                transform=self.test_transform,
                seed=self.capcha_test_seed,
                pregenerate=True,
            )
        else:
            self.val_data = IIIT5k(
                self.data_dir,
                train=False,
                lexicon=self.lexicon,
                transform=self.test_transform,
            )
            if self.capcha_test_samples:
                self.val_data = ConcatDataset(
                    (
                        self.val_data,
                        CapchaDataset(
                            (3, 15),
                            samples=self.capcha_test_samples,
                            transform=self.test_transform,
                            seed=self.capcha_test_seed,
                            pregenerate=True,
                        ),
                    )
                )

    def train_dataloader(self):
        return DataLoader(
            self.train_data,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            collate_fn=ocr_collate_fun,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_data,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            collate_fn=ocr_collate_fun,
        )
