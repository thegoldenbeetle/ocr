from enum import Enum
from pathlib import Path
from typing import Optional, Union

import scipy.io
from PIL import Image
from torch.utils.data import Dataset

from .common import OCRDataBatch


class IIIT5kLexicon(Enum):
    SMALL = "smallLexi"
    MEDIUM = "mediumLexi"


class IIIT5k(Dataset):
    def __init__(
        self,
        dataset_path: Union[str, Path],
        train: bool = True,
        lexicon: Optional[str] = "smallLexi",
        transform: Optional = None,
    ):
        self.dataset_path = Path(dataset_path)
        if lexicon:
            self.lexicon = IIIT5kLexicon(lexicon)
        else:
            self.lexicon = set()
        self.transform = transform
        self.split = "train" if train else "test"
        self.data = scipy.io.loadmat(self.dataset_path / f"{self.split}data.mat")[
            f"{self.split}data"
        ][0]

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        info = self.data[idx]
        img_path = self.dataset_path / info["ImgName"][0]
        orig_img = Image.open(img_path)
        if self.transform:
            img = self.transform(orig_img)
        text = info["GroundTruth"][0]
        lexicon = {item[0] for item in info[self.lexicon.value][0]}
        return OCRDataBatch(
            img=img,
            orig_img=[orig_img],
            text=[text],
            lexicon=[lexicon],
        )
