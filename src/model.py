from typing import List, Optional, Set

import lightning as L
import numpy as np
import torch
from sklearn.preprocessing import LabelEncoder
from torch import nn, optim
from torchmetrics import CharErrorRate

from src.utils import decode_raw_text


class CRNN(L.LightningModule):
    def __init__(
        self,
        alphabet: str = "0123456789abcdefghijklmnopqrstuvwxyz",
        space_symbol: str = "-",
        log_img_every: int = 10,
    ):
        super().__init__()
        self.save_hyperparameters()

        self.log_img_every = log_img_every
        self.space_symbol = space_symbol
        self.charencoder = LabelEncoder()
        self.charencoder.fit(list(space_symbol + alphabet))
        num_classes = len(self.charencoder.classes_)

        self.convolution = nn.Sequential(
            nn.Conv2d(1, 64, kernel_size=3, padding=1, stride=1),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=2),
            nn.Conv2d(64, 128, 3, padding=1, stride=1),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=2),
            nn.Conv2d(128, 256, 3, padding=1, stride=1),
            nn.ReLU(),
            nn.Conv2d(256, 256, 3, padding=1, stride=1),
            nn.ReLU(),
            nn.MaxPool2d((1, 2), stride=2),
            nn.Conv2d(256, 512, 3, padding=1, stride=1),
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.Conv2d(512, 512, 3, padding=1, stride=1),
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.MaxPool2d((1, 2), stride=2),
            nn.Conv2d(512, 512, 2, padding=0, stride=1),
            nn.ReLU(),
        )
        self.rnn = nn.LSTM(
            input_size=512,
            hidden_size=256,
            num_layers=2,
            bidirectional=True,
            batch_first=True,
        )
        self.class_fc = nn.Linear(256 * 2, num_classes)

        self.val_cer = CharErrorRate()
        self.val_step_acc = []

    def forward(self, x):
        x = self.convolution(x)
        x = x.squeeze(2).permute(0, 2, 1)
        x, _ = self.rnn(x)
        x = self.class_fc(x)
        x = nn.functional.softmax(x, dim=2)
        return x

    def _ctc_loss(self, pred, targets):
        input_lengths = torch.full((pred.shape[0],), pred.shape[1]).to(self.device)
        target_lengths = torch.tensor(np.array([len(target) for target in targets])).to(
            self.device
        )
        target_lengths.max()
        targets_tensor = torch.tensor(
            np.concatenate(
                [
                    self.charencoder.transform(
                        list(
                            (
                                target
                                # + self.space_symbol * (max_target_length - len(target))
                            ).lower()
                        )
                    )
                    for target in targets
                ]
            )
        ).to(self.device)
        return nn.functional.ctc_loss(
            torch.log(pred.permute(1, 0, 2)),
            targets_tensor,
            input_lengths,
            target_lengths,
            blank=self.charencoder.transform(["-"])[0],
        )

    def decode_word(self, preds, lexicon: Optional[Set[str]] = None) -> List[str]:
        if lexicon:
            raise NotImplementedError
        else:
            preds = preds.detach().cpu()
            raw_words = [
                "".join(self.charencoder.inverse_transform([x.argmax() for x in pred]))
                for pred in preds
            ]
            words = [decode_raw_text(word) for word in raw_words]
            return words

    def training_step(self, batch, batch_idx):
        del batch_idx
        batch_size = len(batch.text)
        pred = self(batch.img)
        loss = self._ctc_loss(pred, batch.text)
        self.log("train/loss", loss, prog_bar=True, batch_size=batch_size)
        return loss

    def validation_step(self, batch, batch_idx):
        batch_size = len(batch.text)
        pred = self(batch.img)
        loss = self._ctc_loss(pred, batch.text)

        self.log("val/loss", loss, prog_bar=True, batch_size=batch_size)

        pred_words = self.decode_word(pred)
        self.val_cer(pred_words, batch.text)
        self.log(
            "val/CER",
            self.val_cer,
            on_step=False,
            on_epoch=True,
            batch_size=batch_size,
        )

        val_acc = [p == t for p, t in zip(pred_words, batch.text)]
        self.val_step_acc.extend(val_acc)

        if batch_idx % self.log_img_every == 0:
            mode = "HWC" if len(np.array(batch.orig_img[0]).shape) == 3 else "HW"
            self.logger.experiment.add_image(
                f"example_{batch_idx}/img",
                np.array(batch.orig_img[0]),
                self.current_epoch,
                dataformats=mode,
            )
            self.logger.experiment.add_text(
                f"example_{batch_idx}/raw_text",
                pred_words[0],
                self.current_epoch,
            )
        return loss

    def on_validation_epoch_end(self):
        self.log("val/accuracy", np.mean(self.val_step_acc))
        self.val_step_acc.clear()

    def configure_optimizers(self):
        optimizer = optim.Adadelta(self.parameters())
        return optimizer
