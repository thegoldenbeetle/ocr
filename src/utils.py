import itertools


def decode_raw_text(text, gap_symbol="-"):
    return "".join([ch for ch, _ in itertools.groupby(text) if ch != gap_symbol])
